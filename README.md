# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://ZanHorvat@bitbucket.org/ZanHorvat/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/ZanHorvat/stroboskop/commits/8ab27cad9ca3432c51c6b19e01cefba96279c0cd

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/ZanHorvat/stroboskop/commits/0f25cd69568d394d7a94dae1ffd4fbe4807eaf83?at=izgled

Naloga 6.3.2:
https://bitbucket.org/ZanHorvat/stroboskop/commits/d4ee51d798cdb379411c5bdf6f2352b0dae779df?at=izgled

Naloga 6.3.3:
https://bitbucket.org/ZanHorvat/stroboskop/commits/cdbd36e89db58b3d26bd2b82c168bc32b6956a1a?at=izgled

Naloga 6.3.4:
https://bitbucket.org/ZanHorvat/stroboskop/commits/49bdab222182f817fdf6f03fee69a352c244a7f3?at=izgled

Naloga 6.3.5:

```
git chechout master
git merge izgled
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/ZanHorvat/stroboskop/commits/32bc582d028e8ccc0943e75149f73c11b2c862fd

Naloga 6.4.2:
https://bitbucket.org/ZanHorvat/stroboskop/commits/f99a00ec82444cfbb93674a866f090c3db76c753

Naloga 6.4.3:
https://bitbucket.org/ZanHorvat/stroboskop/commits/b010aa5722633d68365e89a0a62c74e1621aee48

Naloga 6.4.4:
https://bitbucket.org/ZanHorvat/stroboskop/commits/ac8200bcf416b17251f8c68384f85bf876d1acd6